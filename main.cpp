#include<iostream>
#include<sstream>
#include <windows.h>

#define ID_TIMERA 1

using namespace std;

HWND wartosc_a, wartosc_vp;
HWND kalkulator_a, kalkulator_v, kalkulator_wynik;
double a = 0;
double vp = 0;
double vk = 0;
double v = 0;
double t = 0;
double s = 0;
RECT rcKulka;
RECT rcOkno;
HBITMAP hbmKulka;
BITMAP bmKulka;
HDC hdcMem;
HBITMAP hbmOld;
RECT Prostokat;


HDC hdc;

double podaj_liczbe(HWND &h)
{
	DWORD dlugosc = GetWindowTextLength(h);
	LPSTR zmienna = (LPSTR)GlobalAlloc(GPTR, dlugosc + 1);
	GetWindowText(h, zmienna, dlugosc + 1);
	string x = zmienna;
	double d;
	d = atof(x.c_str());
	return d;
}

/* This is where all the input to the window goes to */
LRESULT CALLBACK WndProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam) {
	switch(Message) {
		
		/* Upon destruction, tell the main thread to stop */
		case WM_DESTROY: {
			PostQuitMessage(0);
			break;
		}
		
		case WM_COMMAND: 
		{
			switch(wParam)
			{
				case 1: {
					vp = podaj_liczbe(wartosc_vp);
					a = podaj_liczbe(wartosc_a);
					
					hdc = GetDC(hwnd);
					HBRUSH pedzel = CreateSolidBrush(0x1111FF);
					HBRUSH stary = (HBRUSH)SelectObject(hdc, pedzel);
					Rectangle(hdc, 320, 170, 410, 420);
					SelectObject(hdc, stary);
					DeleteObject(pedzel);
					DeleteObject(stary);
					ReleaseDC(hwnd, hdc);
					
					SetTimer(hwnd, ID_TIMERA, 1, NULL);
					break;
				}
				
				case 2: {
					double a;
					double v;
					a = podaj_liczbe(kalkulator_a);
					v = podaj_liczbe(kalkulator_v);
					
					double wynik = (v*v)/(2*a);
					ostringstream s;
					s << wynik;
					string x = s.str();
					SetWindowText(kalkulator_wynik, x.c_str());
					
					
					break;
				}
			}
			break;
		}
		
		case WM_TIMER: {
			s = vp*t-((a*t*t)/2);
			v = vp-(t*a);
			hdc = GetDC(hwnd);
			if(rcKulka.left >= 740-bmKulka.bmWidth)
				rcKulka.left = 0;
				
			ostringstream ss;
			string x;
			
			ss << v;
			x = "Aktualna pr�dko��: "+ss.str();
			TextOut(hdc, 20, 20, x.c_str(), x.size());
			
			ostringstream ss2;
			ss2 << t;
			x = "Czas: "+ss2.str();
			TextOut(hdc, 350, 20, x.c_str(), x.size());
			
			ostringstream ss3;
			ss3 << s;
			x = "Przebya droga: "+ss3.str();
			TextOut(hdc, 480, 20, x.c_str(), x.size());

			FillRect(hdc, &Prostokat, (HBRUSH)(COLOR_WINDOW+1));
			rcKulka.left = (int)(s)%(740-bmKulka.bmWidth);
			BitBlt(hdc, rcKulka.left, rcKulka.top, bmKulka.bmWidth, bmKulka.bmHeight, hdcMem, 0, 0, SRCCOPY);
			
			t = t + 0.01;
			
			RECT rc;
			SetRect(&rc, 320, 170, 410, 420);
			FillRect(hdc, &rc, (HBRUSH)(COLOR_WINDOW+1));
			HBRUSH pedzel = CreateSolidBrush(0x1111FF);
			HBRUSH stary = (HBRUSH)SelectObject(hdc, pedzel);
			int pasek = 250*((vp-v)/100);
			Rectangle(hdc, 410, 420, 320, 170+pasek);
//			Rectangle(hdc, 320, 170, 410, 420);
			SelectObject(hdc, stary);
			
			if(v <= 0)
			{
				TextOut(hdc, 20, 20, "Aktualna pr�dko��: 0                         ", 45);
				KillTimer(hwnd, ID_TIMERA);	
			}
			ReleaseDC(hwnd, hdc);	
			break;
		}
		
		/* All other messages (a lot of them) are processed using default procedures */
		default:
			return DefWindowProc(hwnd, Message, wParam, lParam);
	}
	return 0;
}

/* The 'main' function of Win32 GUI programs: this is where execution starts */
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	WNDCLASSEX wc; /* A properties struct of our window */
	HWND hwnd; /* A 'HANDLE', hence the H, or a pointer to our window */
	MSG msg; /* A temporary location for all messages */

	/* zero out the struct and set the stuff we want to modify */
	memset(&wc,0,sizeof(wc));
	wc.cbSize		 = sizeof(WNDCLASSEX);
	wc.lpfnWndProc	 = WndProc; /* This is where we will send messages to */
	wc.hInstance	 = hInstance;
	wc.hCursor		 = LoadCursor(NULL, IDC_ARROW);
	
	/* White, COLOR_WINDOW is just a #define for a system color, try Ctrl+Clicking it */
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	wc.lpszClassName = "WindowClass";
	wc.hIcon		 = LoadIcon(NULL, IDI_APPLICATION); /* Load a standard icon */
	wc.hIconSm		 = LoadIcon(NULL, IDI_APPLICATION); /* use the name "A" to use the project icon */

	if(!RegisterClassEx(&wc)) {
		MessageBox(NULL, "Window Registration Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
		return 0;
	}

	hwnd = CreateWindowEx(WS_EX_CLIENTEDGE,"WindowClass","Projekt z fizyki komputerowej",WS_VISIBLE|WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, /* x */
		CW_USEDEFAULT, /* y */
		760, /* width */
		480, /* height */
		NULL,NULL,hInstance,NULL);
	
	wartosc_a = CreateWindowEx(WS_EX_CLIENTEDGE,"EDIT",NULL,WS_VISIBLE | WS_CHILD | ES_NUMBER,
		40, /* x */
		220, /* y */
		230, /* width */
		20, /* height */
		hwnd,NULL,hInstance,NULL);
	wartosc_vp = CreateWindowEx(WS_EX_CLIENTEDGE,"EDIT",NULL,WS_VISIBLE | WS_CHILD | ES_NUMBER,
		40, /* x */
		290, /* y */
		230, /* width */
		20, /* height */
		hwnd,NULL,hInstance,NULL);
	
	CreateWindowEx(WS_EX_CLIENTEDGE, "BUTTON", "Symulacja",WS_VISIBLE | WS_CHILD,
		80, /* x */
		350, /* y */
		150, /* width */
		30, /* height */
		hwnd,(HMENU)1,hInstance,NULL);
	
	
	kalkulator_a = CreateWindowEx(WS_EX_CLIENTEDGE,"EDIT",NULL,WS_VISIBLE | WS_CHILD | ES_NUMBER,
		450, /* x */
		220, /* y */
		230, /* width */
		20, /* height */
		hwnd,NULL,hInstance,NULL);
	kalkulator_v = CreateWindowEx(WS_EX_CLIENTEDGE,"EDIT",NULL,WS_VISIBLE | WS_CHILD | ES_NUMBER,
		450, /* x */
		290, /* y */
		230, /* width */
		20, /* height */
		hwnd,NULL,hInstance,NULL);
	kalkulator_wynik = CreateWindowEx(WS_EX_CLIENTEDGE,"EDIT",NULL,WS_VISIBLE | WS_CHILD | ES_NUMBER,
		450, /* x */
		340, /* y */
		230, /* width */
		20, /* height */
		hwnd,NULL,hInstance,NULL);
		
	CreateWindowEx(WS_EX_CLIENTEDGE, "BUTTON", "Oblicz drog� hamowania",WS_VISIBLE | WS_CHILD,
		480, /* x */
		380, /* y */
		170, /* width */
		30, /* height */
		hwnd,(HMENU)2,hInstance,NULL);
	
	hdc = GetDC(hwnd);
	HPEN czarny_mazak = CreatePen( PS_SOLID, 1 , RGB(0, 0, 0) );
	HPEN stary_mazak = (HPEN)SelectObject(hdc, czarny_mazak);
	
	// Wst�pne wypisanie warto�� V(pr�dko��), a(op�nienie) oraz s(droga)
	TextOut(hdc, 20, 20, "Aktualna pr�dko��: 0", 20);
	TextOut(hdc, 350, 20, "Czas: 0", 7);
	TextOut(hdc, 480, 20, "Przebyta droga: 0", 17);
	
	// Prostok�ty do grupowania kontrolek
	Rectangle(hdc, 20, 170, 300, 420);
	Rectangle(hdc, 430, 170, 710, 420);
	
	// Licznik
	Rectangle(hdc, 320, 170, 410, 420);
	
	// Podpisy wewn�trz prostok�t�w
	TextOut(hdc, 40, 190, "Podaj warto�� a (warto�� op�nienia):", 37);
	TextOut(hdc, 40, 260, "Podaj warto�� Vp (warto�� pocz�tkowa):", 38);
	
	// Kalkulator
	TextOut(hdc, 450, 190, "Podaj warto�� a (op�nienie):", 27);
	TextOut(hdc, 450, 260, "Podaj warto�� V (pr�dko��):", 28);
	TextOut(hdc, 450, 320, "Wynik:", 6);
	
	SelectObject(hdc, stary_mazak);
	ReleaseDC(hwnd, hdc);
	
	GetClientRect(hwnd, &rcOkno);
	hbmKulka = (HBITMAP) LoadImage(NULL, "img/samochod.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(hbmKulka, sizeof(bmKulka), &bmKulka);
	
	hdc = GetDC(hwnd);
	hdcMem = CreateCompatibleDC(hdc);
	hbmOld = (HBITMAP)SelectObject(hdcMem, hbmKulka);
	
	ReleaseDC(hwnd, hdc);
	
	SetRect(&rcKulka, 0, 50, 0+bmKulka.bmWidth, 50+bmKulka.bmHeight);
	SetRect(&Prostokat, 0, 50, 770, 150);
	
	if(hwnd == NULL) {
		MessageBox(NULL, "Window Creation Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
		return 0;
	}
	
	while(GetMessage(&msg, NULL, 0, 0) > 0) { /* If no error is received... */
		TranslateMessage(&msg); /* Translate key codes to chars if present */
		DispatchMessage(&msg); /* Send it to WndProc */
	}
	return msg.wParam;
}
